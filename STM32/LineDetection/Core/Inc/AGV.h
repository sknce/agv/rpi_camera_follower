/*
 * AGV.h
 *
 *  Created on: Feb 14, 2024
 *      Author: yeti2
 */

#ifndef INC_AGV_H_
#define INC_AGV_H_
#include "main.h"
class AGV {
	public:
		AGV();
		virtual ~AGV();
		void init(FDCAN_HandleTypeDef &hfdcan, FDCAN_TxHeaderTypeDef &TxHeader1);
		void motion(float deviation[2]);
		void pixel(float pixel_calculate);
private:
		FDCAN_HandleTypeDef hfdcan1;
		FDCAN_TxHeaderTypeDef TxHeader;
		float pixel_size = 0.00;
		float AGV_control_vector[3]= {0.0, 0.0, 0.0};
		float member=0.0;
		void calculate( float deviation[2]);
		void AGV_move_forward(float dimention_x, float dimention_y);
		void AGV_move_right();
		void AGV_move_left();
		void AGV_stop();
		void convertToArray(uint8_t output[24], float control_vector[3]);
		void send(float control_vector[3]);


};

#endif /* INC_AGV_H_ */
