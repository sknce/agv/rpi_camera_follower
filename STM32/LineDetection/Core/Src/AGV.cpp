/*
 * AGV.c
 *
 *  Created on: Feb 14, 2024
 *      Author: yeti2
 */
#include "AGV.h"
#include <math.h>
#include <cstring>
#include "main.h"
AGV::AGV() {
	// TODO Auto-generated constructor stub

}

AGV::~AGV() {
	// TODO Auto-generated destructor stub
}


	void AGV::init(FDCAN_HandleTypeDef &hfdcan, FDCAN_TxHeaderTypeDef &TxHeader1){
		hfdcan1=hfdcan;
		TxHeader=TxHeader1;
	}
	void AGV::motion(float deviation[2]){
		calculate(deviation);
	}

	void AGV::calculate(float deviation[2]){

		if(deviation[1] >= member){

			AGV_move_forward(deviation[0], deviation[1]);
			member=deviation[0];

		} else{
			if (deviation[0] > 0.0){

				AGV_move_right();
			} else if (deviation[0] < 0.0){

				AGV_move_left();
			} else {
				AGV_stop();
			}
		}

	}


	void AGV::AGV_move_forward(float dimention_x, float dimention_y){
		float speed = 0.5; // m/s
		float camera_holder_distance = 0.05;
		float AGV_center_y_distance = dimention_y + 0.44 + camera_holder_distance;
		float AGV_center_distance = sqrt(AGV_center_y_distance * AGV_center_y_distance + dimention_x * dimention_x);

		AGV_control_vector[0] =  speed * dimention_x/AGV_center_distance;
		AGV_control_vector[1]= speed * AGV_center_y_distance / AGV_center_distance;
		AGV_control_vector[2] = asin(dimention_x/AGV_center_distance); // rad

		send(AGV_control_vector);
	}
	void AGV::AGV_move_right(){

		float initial_speed=0.4;
		int initial_ramp=5;
		float jump = initial_speed/initial_ramp;

		for(int i = initial_ramp; i>0; i--){

			AGV_control_vector[0]=0.0;
			AGV_control_vector[1]=initial_speed;
			AGV_control_vector[2]=0.0;
			initial_speed=initial_speed-jump;
			send(AGV_control_vector);
		}

		AGV_control_vector[0]=0.0;
		AGV_control_vector[1]=0.0;
		AGV_control_vector[2]=1.570796326795;

	}
	void AGV::AGV_move_left(){

			float initial_speed=0.4;
			int initial_ramp=5;
			float jump = initial_speed/initial_ramp;

			for(int i = initial_ramp; i>0; i--){

				AGV_control_vector[0]=0.0;
				AGV_control_vector[1]=initial_speed;
				AGV_control_vector[2]=0.0;
				initial_speed=initial_speed-jump;
				send(AGV_control_vector);
			}

			AGV_control_vector[0]=0.0;
			AGV_control_vector[1]=0.0;
			AGV_control_vector[2]=-1.570796326795;
			send(AGV_control_vector);
		}


	void AGV::AGV_stop(){
		AGV_control_vector[0]=0.0;
		AGV_control_vector[1]=0.0;
		AGV_control_vector[2]=0.0;
		send(AGV_control_vector);
	}
	void AGV::convertToArray(uint8_t output[24], float control_vector[3]) {
	    static_assert(sizeof(uint8_t[24]) >= sizeof(float[3]), "output array size must be at least 24 bytes");
	    memcpy(output, control_vector, sizeof(float[3]));
	    memset(output + sizeof(float[3]), 0, sizeof(uint8_t[24]) - sizeof(float[3]));
	}

	void AGV::send(float control_vector[3]){

		uint8_t u_control_vector[24];
		convertToArray(u_control_vector, control_vector );

		if (HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &TxHeader, u_control_vector) != HAL_OK)
				{
					Error_Handler();
				}
				HAL_Delay(1);
	}
