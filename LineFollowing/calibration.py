import cv2
import np
import depthai as dai

def nothing(a):
    pass

def initializeTrackbars(intialTracbarVals,wT=480, hT=240):
    cv2.namedWindow("Trackbars")
    cv2.resizeWindow("Trackbars", 360, 240)
    cv2.createTrackbar("Width Top", "Trackbars", intialTracbarVals[0],wT//2, nothing)
    cv2.createTrackbar("Height Top", "Trackbars", intialTracbarVals[1], hT, nothing)
    cv2.createTrackbar("Width Bottom", "Trackbars", intialTracbarVals[2],wT//2, nothing)
    cv2.createTrackbar("Height Bottom", "Trackbars", intialTracbarVals[3], hT, nothing)
 
def warpImg (img,points,w,h,inv=False):
    pts1 = np.float32(points)
    pts2 = np.float32([[0,0],[w,0],[0,h],[w,h]])
    if inv:
        matrix = cv2.getPerspectiveTransform(pts2,pts1)
    else:
        matrix = cv2.getPerspectiveTransform(pts1,pts2)
    imgWarp = cv2.warpPerspective(img,matrix,(w,h))
    return imgWarp

def calibration(img):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # SHI-TOMASI METHOD
    corners = cv2.goodFeaturesToTrack(gray_img, maxCorners=50,
                    qualityLevel=0.15, minDistance=50)
    corners = np.int0(corners)

    for c in corners:
        x, y = c.ravel()
        img = cv2.circle(img, center=(x, y), radius=20, 
                        color=(0, 0, 255), thickness=-1)


    #  # SHI-TOMASI METHOD
    # corners = cv2.goodFeaturesToTrack(gray_img, maxCorners=50,
    #                 qualityLevel=0.15, minDistance=50)
    # corners = np.int0(corners)

    # for c in corners:
    #     x, y = c.ravel()
    #     img = cv2.circle(img, center=(x, y), radius=20, 
    #                     color=(0, 0, 255), thickness=-1)
        
    # # HARRIS CORNER DETECTION
    # corners = cv2.goodFeaturesToTrack(gray_img, maxCorners=50,
    #                 qualityLevel=0.01, minDistance=50,
    #                 useHarrisDetector=True, k=0.1)
    # corners = np.int0(corners)

    # for c in corners:
    #     x, y = c.ravel()
    #     img = cv2.circle(img, center=(x, y), radius=10, 
    #                     color=(0, 254, 0), thickness=-1)

    cv2.imshow("Orginal", img)
# Create pipeline
pipeline = dai.Pipeline()

# Define source and outputs
camRgb = pipeline.create(dai.node.ColorCamera)
xoutPreview = pipeline.create(dai.node.XLinkOut)

xoutPreview.setStreamName("preview")

# Properties
camRgb.setPreviewSize(500, 500)
camRgb.setBoardSocket(dai.CameraBoardSocket.CAM_A)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(True)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

# Linking
camRgb.preview.link(xoutPreview.input)
# Connect to device and start pipeline
with dai.Device(pipeline) as device:
    preview = device.getOutputQueue('preview', maxSize=1, blocking=False)
    while True:
        inPreview = preview.get()

        if inPreview is not None:
            frame = inPreview.getCvFrame()
            calibration(frame)

        if cv2.waitKey(1) == ord('q'):
            break