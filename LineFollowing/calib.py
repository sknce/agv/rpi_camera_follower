import cv2
import numpy as np
import depthai as dai


def find_color(image, lower_color, upper_color):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower_color, upper_color)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    if contours:
        largest_contour = max(contours, key=cv2.contourArea)
        x, y, w, h = cv2.boundingRect(largest_contour)
        center_x = x + w // 2
        center_y = y + h //2
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        return center_x, center_y
    else:
        return None
    
lower_yellow = np.array([0, 100, 100])
upper_yellow = np.array([25, 255, 255])

lower_blue = np.array([100, 100, 100])
upper_blue = np.array([140, 255, 255])

lower_green = np.array([40, 40, 40])
upper_green = np.array([80, 255, 255])

lower_red = np.array([170, 40, 40])
upper_red = np.array([180, 255, 255])
# Create pipeline
pipeline = dai.Pipeline()

# Define source and outputs
camRgb = pipeline.create(dai.node.ColorCamera)
xoutPreview = pipeline.create(dai.node.XLinkOut)

xoutPreview.setStreamName("preview")

# Properties
camRgb.setPreviewSize(500, 500)
camRgb.setBoardSocket(dai.CameraBoardSocket.CAM_A)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(True)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

# Linking
camRgb.preview.link(xoutPreview.input)

# Connect to device and start pipeline
with dai.Device(pipeline) as device:
    preview = device.getOutputQueue('preview', maxSize=1, blocking=False)
  

    while True:
        inPreview = preview.get()

        if inPreview is not None:
            frame = inPreview.getCvFrame()
            processed_frame = frame.copy()
            tape_center = find_color(processed_frame, lower_green, upper_greenq)
            
            if tape_center:
                cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)
                
            cv2.imshow("Processed Frame", processed_frame)
      
        if cv2.waitKey(1) == ord('q'):
            break

cv2.destroyAllWindows()
spi.close()


