import cv2
import numpy as np
import spidev
import time
import depthai as dai
import struct

def find_yellow_tape(image):
    # Konwersja obrazu do przestrzeni kolorów HSV
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # Zakres kolorów żółtych w przestrzeni HSV
    lower_yellow = np.array([0, 100, 100])
    upper_yellow = np.array([25, 255, 255])

    # Progowanie obrazu w celu wykrycia żółtej taśmy
    mask = cv2.inRange(hsv, lower_yellow, upper_yellow)

    # Znajdowanie konturów na masce
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if contours:
        # Wybieramy największy kontur (możemy zmienić to kryterium)
        largest_contour = max(contours, key=cv2.contourArea)
        
        # Obliczamy prostokąt otaczający kontur
        x, y, w, h = cv2.boundingRect(largest_contour)

        # Znajdujemy środek prostokąta
        center_x = x + w // 2
        center_y = y 

        # Rysujemy prostokąt otaczający kontur na oryginalnym obrazie
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

        return center_x, center_y
    else:
        return None

def calculate_deviation(image_width, image_height, center_x, center_y):
    # Obliczanie odchylenia od środka kamery
    deviation_x = center_x - image_width // 2
    deviation_y =  - center_y + image_height
    return deviation_x, deviation_y
    
def float_to_bytes(float_num_x):
    return bytearray(struct.pack('f', float_num_x))
    
# # Inicjalizacja SPI
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = 1000000
spi.mode = 0b01


# Create pipeline
pipeline = dai.Pipeline()

# Define source and outputs
camRgb = pipeline.create(dai.node.ColorCamera)
xoutPreview = pipeline.create(dai.node.XLinkOut)

xoutPreview.setStreamName("preview")

# Properties
camRgb.setPreviewSize(500, 500)
camRgb.setBoardSocket(dai.CameraBoardSocket.CAM_A)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(True)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

# Linking
camRgb.preview.link(xoutPreview.input)

# Connect to device and start pipeline
with dai.Device(pipeline) as device:
    preview = device.getOutputQueue('preview', maxSize=1, blocking=False)
    
    pixel = 0.1
    
   
    

    while True:
        inPreview = preview.get()

        if inPreview is not None:
            frame = inPreview.getCvFrame()
            processed_frame = frame.copy()
     
            tape_center = find_yellow_tape(processed_frame)

            if tape_center:
                # Oblicz odchylenie od środka kamery
                (image_height, image_width) = processed_frame.shape[:2]
                deviation = calculate_deviation(image_width, image_height, tape_center[0], tape_center[1])
                
                
                # wysłanie przez SPI
                float_data_x = deviation[0] * pixel
                float_data_y = deviation[1] * pixel
                float_bytes_x = float_to_bytes(float_data_x)
                float_bytes_y = float_to_bytes(float_data_y)
                data = float_bytes_x + float_bytes_y
                spi.xfer(data)
        
                #data1= struct.pack('<h', deviation[0])
                #data2= struct.pack('<h', deviation[1])
                
                #data = data1 + data2
                #spi.xfer(data)
               
                print(float_bytes_y)
                #print(data)
                # Obniż punkt środka kamery o 60 pikseli
                center_y_camera = processed_frame.shape[0] // 2 + 160

                # Wyświetlanie informacji o odchyleniu na obrazie
                cv2.putText(processed_frame, f"Deviation: {deviation}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)

                # Wyświetlanie środka kamery po obniżeniu o 60 pikseli
                cv2.circle(processed_frame, (image_width // 2, center_y_camera), 5, (255, 0, 0), -1)

                # Wyświetlanie środka prostokąta
                cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)

            # Wyświetlanie oryginalnego obrazu oraz obrazu z zaznaczoną żółtą taśmą
        cv2.imshow("Original Frame", frame)
        cv2.imshow("Processed Frame", processed_frame)

        if cv2.waitKey(1) == ord('q'):
            break

cv2.destroyAllWindows()
spi.close()

