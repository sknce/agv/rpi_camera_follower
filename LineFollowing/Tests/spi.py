import spidev
import time
import struct
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = 1000000
spi.mode = 0b01

def float_to_bytes(float_num):
    return struct.pack('f', float_num)
    
while True:
    float_num = 0.1114
    date=float_to_bytes(float_num)
    spi.xfer(date)
    time.sleep(0.2)
           
spi.close()
