import cv2
import numpy as np
import depthai as dai

def find_color(image, lower_color, upper_color):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower_color, upper_color)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    if contours:
        largest_contour = max(contours, key=cv2.contourArea)
        x, y, w, h = cv2.boundingRect(largest_contour)
        center_x = x + w // 2
        center_y = y + h //2
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        return center_x, center_y
    else:
        return None

# Define lower and upper color bounds
lower_yellow = np.array([0, 100, 100])
upper_yellow = np.array([25, 255, 255])

lower_blue = np.array([150, 60, 20])
upper_blue = np.array([255, 0, 0])

lower_green = np.array([40, 40, 40])
upper_green = np.array([80, 255, 255])

lower_red = np.array([40, 40, 170])
upper_red = np.array([0, 0, 255])

# Create pipeline
pipeline = dai.Pipeline()

# Define source and outputs
camRgb = pipeline.create(dai.node.ColorCamera)
xoutPreview = pipeline.create(dai.node.XLinkOut)
xoutPreview.setStreamName("preview")

# Properties
camRgb.setPreviewSize(500, 500)
camRgb.setBoardSocket(dai.CameraBoardSocket.CAM_A)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(True)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

# Linking
camRgb.preview.link(xoutPreview.input)

# Connect to device and start pipeline
with dai.Device(pipeline) as device:
    preview = device.getOutputQueue('preview', maxSize=1, blocking=False)
    
    yellow_x, yellow_y = 0, 0
    red_x, red_y = 0, 0
    blue_x, blue_y = 0, 0
    green_x, green_y = 0, 0
    y = 0
    b = 0
    r = 0
    g = 0
    
    while True:
        inPreview = preview.get()

        if inPreview is not None:
            frame = inPreview.getCvFrame()
            processed_frame = frame.copy()
            
            # Find yellow color
            tape_center = find_color(processed_frame, lower_yellow, upper_yellow)
            if tape_center:
                yellow_x += tape_center[0]
                yellow_y += tape_center[1]
                y += 1
                cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)

            # Find blue color
            tape_center = find_color(processed_frame, lower_blue, upper_blue)
            if tape_center:
                blue_x += tape_center[0]
                blue_y += tape_center[1]
                b += 1
                cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)

            # Find green color
            tape_center = find_color(processed_frame, lower_green, upper_green)
            if tape_center:
                green_x += tape_center[0]
                green_y += tape_center[1]
                g += 1
                cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)

            # Find red color
            tape_center = find_color(processed_frame, lower_red, upper_red)
            if tape_center:
                red_x += tape_center[0]
                red_y += tape_center[1]
                r += 1
                cv2.circle(processed_frame, (tape_center[0], tape_center[1]), 5, (0, 0, 255), -1)

          
            cv2.imshow("Processed Frame", processed_frame)

        if cv2.waitKey(1) == ord('q'):
            break


    # Calculate average centers
    if y != 0:
        yellow_x /= y
        yellow_y /= y
    if b != 0:
        blue_x /= b
        blue_y /= b
    if g != 0:
        green_x /= g
        green_y /= g
    if r != 0:
        red_x /= r
        red_y /= r


    print("Blue center:", yellow_x, yellow_y)
    print("Blue center:", blue_x, blue_y)
    print("Green center:", green_x, green_y)
    print("Red center:", red_x, red_y)

cv2.destroyAllWindows()
